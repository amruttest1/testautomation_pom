/*
 * Login Logout Methods
 * created by Harsha Shelar
 * 27th feb 2018
*/
package testbase;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;


public class JiraloginTestScript extends cls_browser{
	 
	//WebDriver driver=chromeBrowser();
	public void login()
	 {	
	 	 try
	 	 {	 		
	 		WebElement login = driver.findElement(By.id(prop.getProperty("login.username")));
			 login.sendKeys(prop.getProperty("username"));
			 Thread.sleep(2000);
			 WebElement password = driver.findElement(By.id(prop.getProperty("login.password")));
				password.sendKeys(prop.getProperty("password"));
				Thread.sleep(2000);
				WebElement loginbtn = driver.findElement(By.name(prop.getProperty("login.submit")));
				loginbtn.click();
				Thread.sleep(4000);
			
	 	 }
	 	catch(Exception e)
	 	{
	 		System.out.println(e.toString());
	 	}
	}
	 public void logout()
	 {
	 	try{
	 		Thread.sleep(2000);
	 	 WebElement admin = driver.findElement(By.xpath(prop.getProperty("logout.admin")));
	 	//*[@id="log_out"]
	 	 Thread.sleep(2000);
	 	 admin.click();
	 	//System.out.println("Click on logout button...");
	 	 driver.findElement(By.xpath(prop.getProperty("logout"))).sendKeys(Keys.ENTER);
	
	 	}
	 	catch(Exception e)
	 	{
	 		System.out.println("Logout successfully...... ");
	 		
	 	}
	 		}

	 
}
