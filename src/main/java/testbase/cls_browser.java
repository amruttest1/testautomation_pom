/*
 * Browser class 
 * created by Harsha Shelar
 * 27th feb 2018
*/

package testbase;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class cls_browser extends testproperties {
      static WebDriver driver ;
	
	public static WebDriver chromeBrowser() 
	 { 	
		System.out.println(prop.getProperty("URL"));
	 		//driver= new ChromeDriver();	
			if(driver == null){
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver = new ChromeDriver();			
			String baseUrl= prop.getProperty("URL");
					//"http://demo.amrutsoftware.com:8080";
			 driver.get(baseUrl);
			}
			return driver;
	 	}
}
