/*
 * this file is to call properties from the config 
 * created by Harsha Shelar
 * 27th feb 2018
*/
package testbase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeClass;

public class testproperties {
	
	static Properties prop;
	// static Properties prop1;
	 static File f;
	 static FileReader OBJ;
	 
	 public static void setup() throws IOException
	 {
		 prop =new Properties();
		 f= new File(System.getProperty("user.dir")+"//src//main//java//config//config.properties");
		OBJ= new FileReader(f);
		 prop.load(OBJ);
		 	 
		 f= new File(System.getProperty("user.dir")+"//src//main//java//config//locator.properties");
		 OBJ= new FileReader(f);
		 prop.load(OBJ);
	 }
	 
	 @BeforeClass
		public void init() throws IOException{
			setup();
		}
	 
	 public String getobject(String Data) throws IOException
	 {
		setup();
		String  data = prop.getProperty(Data);		
		return data;
	 }
	

}
