package testbase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class JiraLoginTestScript1 {
	 Properties prop;
	 public void LoadData() throws IOException 
	 { 		
		 prop =new Properties();
		 File f= new File(System.getProperty("user.dir"+"..//src//main//java//config//config.properties"));
		 FileReader OBJ= new FileReader(f);
		 prop.load(OBJ);
		 
	 }
	 public String getobject(String Data) throws IOException
	 {
		LoadData();
		String  data = prop.getProperty(Data);
		return data;
	 }

}
