/*
 * testNG class 
 * created by Harsha Shelar
 * 27th feb 2018
*/



package testbase;
import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.ISuite;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class DemoTestSuite extends testproperties
{
	static WebDriver driver;
	
	JiraloginTestScript tc=new JiraloginTestScript();
	  

@Test(priority =0)
  public void Login() throws FileFormatException, IOException, InterruptedException
  {	
	   tc .login();
  }

   @Test(priority =1)
   public void Logout() throws FileFormatException, IOException, InterruptedException
   {	
	   tc .logout();
   }
   @Test(priority =2)
   public void test1() throws FileFormatException, IOException, InterruptedException
   {	
	   String i ="3";
		assertEquals(i, "4");
		  		System.out.println("test1");

   }
  @BeforeClass
  public void beforeMethod() throws InterruptedException {
	  driver=cls_browser.chromeBrowser();
	  }
  @AfterSuite
  public void afterMethod() {
	  
	  driver.quit();
	 
  }
  public void generateReport(List<ISuite> suites, String outputDirectory){}
}

